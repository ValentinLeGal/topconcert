<?php

namespace app\model;

use app\config\Database;
use PDO;

class Model {

    // Attributs
    protected $_table;
    private $_connexion;

    /**
	 * Construit l'appel à la base de données.
	 */
    public function __construct() {
        $db = new Database ();
        $this->_connexion = $db->getConnection ();
    }

    /**
	 * Sélectionne un/des enregristrement(s) en fonction d'un tableau de condition.
	 *
	 * @param array $data de condition(s).
     *
	 * @return array d'enregistrement(s).
	 */
    public function find($data = array()) {
        $conditions = "1";
        $fields = "*";
        $limit = "";
        $order = $this->_table . ".id DESC";
        $othertable = "";
        $join = "";

        if (isset ( $data ["conditions"] )) {
            $conditions = $data ["conditions"];
        }
        if (isset ( $data ["fields"] )) {
            $fields = $data ["fields"];
        }
        if (isset ( $data ["limit"] )) {
            $limit = "LIMIT " . $data ["limit"];
        }
        if (isset ( $data ["order"] )) {
            $order = $data ["order"];
        }
        if (isset ( $data ["othertable"] )) {
            $othertable = ',' . $data ["othertable"];
        }
        if (isset ( $data ["join"] )) {
            $join = "JOIN " . $data ["join"];
        }

        $sql = "SELECT $fields FROM " . $this->_table . " " . $othertable . $join . " WHERE $conditions ORDER BY $order $limit";

        //echo $sql;

        $prepa = $this->_connexion->prepare ( $sql );
        $prepa->execute ();

        $data = $prepa->fetchAll ( PDO::FETCH_ASSOC );

        return $data;
    }
}