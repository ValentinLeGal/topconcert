<?php

namespace app\entity;

class Groupe {
    private $_idGroupe;
    private $_nom;
    private $_genre;
    private $_bio;
    private $_imgAvatar;
    private $_imgCover;

    /**
	 * Constructruir le concert.
	 *
	 * @param array $data
	 */
    public function __construct(array $data) {
        $this->hydrate ( $data );
    }

    /**
	 * Hydrater le constructeur.
	 *
	 * @param array $donnees
	 */
    public function hydrate(array $donnees) {
        foreach ( $donnees as $key => $value ) {
            $method = 'set' . ucfirst ( $key );

            if (method_exists ( $this, $method )) {
                $this->$method ( $value );
            }
        }
    }

    /**
	 * Obtenir l'id du groupe.
	 *
	 * @return int
	 */
    public function getIdGroupe() {
        return $this->_idGroupe;
    }

    /**
	 * Modifier l'id du groupe.
	 *
	 * @param int $_id du groupe
	 */
    public function setIdGroupe($_id) {
        $this->_idGroupe = $_id;
    }

    /**
	 * Obtenir le nom du groupe.
	 *
	 * @return String
	 */
    public function getNom() {
        return $this->_nom;
    }

    /**
	 * Modifier le nom du groupe.
	 *
	 * @param String $_nom
	 *        	du groupe.
	 */
    public function setNom($_nom) {
        $this->_nom = $_nom;
    }

    /**
	 * Obtenir le genre du groupe.
	 *
	 * @return String
	 */
    public function getGenre() {
        return $this->_genre;
    }

    /**
	 * Modifier le genre du groupe.
	 *
	 * @param String $_genre
	 *        	du groupe.
	 */
    public function setGenre($_genre) {
        $this->_genre = $_genre;
    }

    /**
	 * Obtenir la description du groupe.
	 *
	 * @return String
	 */
    public function getBio() {
        return $this->_bio;
    }

    /**
	 * Modifier la description du groupe.
	 *
	 * @param String $_bio
	 *        	du groupe.
	 */
    public function setBio($_bio) {
        $this->_bio = $_bio;
    }

    /**
	 * Obtenir l'adresse de l'avatar du groupe.
	 *
	 * @return String
	 */
    public function getImgAvatar() {
        return $this->_imgAvatar;
    }

    /**
	 * Modifier le nom du fichier de l'avatar du groupe.
	 *
	 * @param String $_imgAvatar
	 *        	du groupe.
	 */
    public function setImgAvatar($_imgAvatar) {
        $this->_imgAvatar = $_imgAvatar;
    }

    /**
	 * Obtenir le nom du fichier de la cover du groupe.
	 *
	 * @return String
	 */
    public function getImgCover() {
        return $this->_imgCover;
    }

    /**
	 * Modifier l'adresse de la cover du groupe.
	 *
	 * @param String $_imgCover
	 *        	du groupe.
	 */
    public function setImgCover($_imgCover) {
        $this->_imgCover = $_imgCover;
    }

    /**
	 * Afficher le groupe.
	 *
	 * @return String
	 */
    public function __toString() {
        return "Id groupe : " . $this->_idGroupe . " | Nom : " . $this->_nom . " | Genre : " . $this->_genre . " | Description : " . $this->_bio . " | Avatar : " . $this->_imgAvatar." | Cover : ".$this->_imgCover;
    }
}
