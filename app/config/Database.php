<?php
namespace app\config;

use PDOException;

class Database {

    private $_host = "localhost";
    private $_db_name = "topconcert";
    private $_username = "root";
    private $_password = "root";
    private $_connec;

    public function getConnection() {
        $this->_connec = null;

        try {
            $this->_connec = new \PDO("mysql:host=" . $this->_host . ";dbname=" . $this->_db_name, $this->_username, $this->_password);
            $this->_connec->exec("set names utf8");
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->_connec;
    }
}