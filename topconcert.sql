-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 16, 2018 at 08:12 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `topconcert`
--

-- --------------------------------------------------------

--
-- Table structure for table `concerts`
--

DROP TABLE IF EXISTS `concerts`;
CREATE TABLE IF NOT EXISTS `concerts` (
  `idConcert` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupe` int(11) NOT NULL,
  `lieu` varchar(64) NOT NULL,
  `ville` varchar(64) NOT NULL,
  `date` date NOT NULL,
  `heure` time NOT NULL,
  `prix` decimal(6,2) NOT NULL,
  PRIMARY KEY (`idConcert`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `concerts`
--

INSERT INTO `concerts` (`idConcert`, `idGroupe`, `lieu`, `ville`, `date`, `heure`, `prix`) VALUES
(1, 1, 'La Poudrière', 'Rochefort', '2018-12-28', '21:30:00', '8.00'),
(2, 2, 'Marennes Art Et Culture', 'Marennes', '2019-02-27', '21:00:00', '5.50'),
(3, 5, 'La Sirène', 'La Rochelle', '2019-02-03', '19:30:00', '15.00'),
(4, 4, 'La sirène', 'La Rochelle', '2019-01-24', '20:45:00', '8.00'),
(5, 3, 'Espace Pierre Mendès France', 'Saintes', '2019-01-10', '21:00:00', '10.00'),
(6, 2, 'Salle de spectacle Jean Gabin', 'Royan', '2019-01-24', '18:55:00', '4.50'),
(7, 4, 'Espace Pierre Mendès France', 'Saintes', '2018-12-20', '21:00:00', '10.50'),
(8, 3, 'La Sirène', 'La Rochelle', '2019-02-04', '21:30:00', '12.00');

-- --------------------------------------------------------

--
-- Table structure for table `groupes`
--

DROP TABLE IF EXISTS `groupes`;
CREATE TABLE IF NOT EXISTS `groupes` (
  `idGroupe` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) NOT NULL,
  `genre` varchar(64) NOT NULL,
  `bio` varchar(1000) NOT NULL,
  `imgAvatar` varchar(64) NOT NULL,
  `imgCover` varchar(64) NOT NULL,
  PRIMARY KEY (`idGroupe`),
  UNIQUE KEY `nom` (`nom`),
  UNIQUE KEY `avatar` (`imgAvatar`),
  UNIQUE KEY `cover` (`imgCover`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groupes`
--

INSERT INTO `groupes` (`idGroupe`, `nom`, `genre`, `bio`, `imgAvatar`, `imgCover`) VALUES
(1, 'Totorro', 'Rock', 'Sur scène, Xavier Rosé (bassiste), Christophe Le Flohic et Jonathan Siche (guitaristes), et Bertrand James (batteur) assurent et se démènent, emportant avec eux le public au gré de leurs envolées de guitares entêtantes, des mélodies qui se répondent et de leurs ruptures de rythmes.', 'totorro-avatar.jpg', 'totorro-cover.jpg'),
(2, 'Mama', 'Rock', 'MAMA, planant sur le rêve de l\'underground de la scène Française actuelle, est un groupe de la génération rock psyché du XXIème siècle. La rencontre entre les frères Granger et le duo Pesta/Tourneur a créé l\'explosion Hiroshimabis, mêlant MA et MA, garage moderne et pop oldschool. Depuis Juillet dernier, ils foncent têtes baissées vers le psyché. Ils concentrent leur énergie vers des performances live intenses et sensuelles.', 'mama-avatar.jpg', 'mama-cover.jpg'),
(3, 'Lysistrata', 'Rock', 'Lysistrata, ces trois amis de vingt ans de moyenne d\'âge, Théo Guéneau (guitare/chant), Max Roy (basse/chant), Ben Amos Cooper (batterie/chant), qui renversent tout sur leur passage depuis quelques mois. Leur musique, en constante évolution depuis leurs débuts en 2013, navigue à plus ou moins égale distance entre noise, post-hardcore, math rock, et post-rock (&quot;post un peu tout&quot; indique leur profil Twitter). Vous lirez ici et là de très flatteuses comparaisons avec certaines références des genres susnommés, comme At the Drive-In, Refused, Battles, Foals, Explosions in the Sky, ou encore Sonic Youth. C\'est souvent vrai mais pas l\'essentiel, l\'influence majeure du groupe semblant être de très loin son envie de l’instant et de l\'endroit, guidée par une spontanéité totale et cette manie insolente de toujours vouloir en découdre.', 'lysistrata-avatar.jpg', 'lysistrata-cover.jpg'),
(4, 'It It Anita', 'Punk', 'It It Anita embrasse le bruit des années 90, mais sait lui donner sa touche personnelle. Ils associent une musique forte et énergique à des détails subtils et des émotions profondes. Pensez à un hybride dynamique de Fugazi, Metz, At The Drive In, Mogwai et Sonic Youth.', 'it-it-anita-avatar.jpg', 'it-it-anita-cover.jpg'),
(5, 'La Jungle', 'Trance', 'Une six-cordes, un casio et un kit de batterie. Il n’en faut parfois pas plus pour faire péter le mercure et irriter les yeux de sueur après deux breaks et trois accords. Jim et Reggie l’ont bien compris et appliquent la fameuse équation 1 + 1 = 3 à leur math/kraut/transe-rock débraillé, qui embarque dans sa frénésie riffs métalliques, giclées de noise et assauts de toms et cymbales guidés au stroboscope. Le rock enfile ses premières sapes pour redevenir sauvage, moite et primal. Deux singes rouquins, un très grand et un plus petit, vous emmènent dans leur milieu naturel : La Jungle !', 'la-jungle-avatar.jpg', 'la-jungle-cover.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `typeUtilisateur` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `motDePasse` varchar(64) NOT NULL,
  PRIMARY KEY (`idUtilisateur`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `utilisateurs`
--

INSERT INTO `utilisateurs` (`idUtilisateur`, `typeUtilisateur`, `email`, `motDePasse`) VALUES
(1, 'admin', 'admin@topconcert.fr', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
