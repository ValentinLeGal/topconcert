<?php

use app\controller\ControllerConcertApi;


// Autoload
function chargerClasse($classe) {
    $classe = str_replace('\\','/',$classe);
    require $classe . '.php';
}

spl_autoload_register('chargerClasse');


// Controller API
$cApi = new ControllerConcertApi();
$cApi->getAllApi();

?>


