var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }


    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");

        if (n > slides.length) {
            slideIndex = slides.length;
        }
        if (n == slides.length && window.innerWidth>1060) {
            document.querySelector("a.next").style.display = "none";
            document.querySelector("a.prev").style.display = "block";
        }

        if (n < 1) {
            slideIndex = slides.length;
        }
        if (n == 1 && window.innerWidth>1060) {
            document.querySelector("a.prev").style.display = "none";
            document.querySelector("a.next").style.display = "block";
        }

        /*if (n > slides.length) {
    slideIndex = 1 //Retour au début du slider
  }

  if (n < 1) {
    slideIndex = slides.length //Retour à la fin du slider
  }*/
        
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none"; 
        }

        slides[slideIndex-1].style.display = "block";
    }