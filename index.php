<?php 
session_start();

//Connexion à la base de données
include 'connexion.php';
$connexion = connexionBd();

//Ajout du fichier fonctions.php
include 'fonctions.php';

//Sélectionner les concerts à venir
$sql1 = "SELECT C.idConcert as idConcert, G.nom as nom, G.genre as genre, G.imgCover as cover, C.ville as ville, C.date as date, C.heure as heure, C.prix as prix FROM concerts C JOIN groupes G ON G.idGroupe = C.idGroupe ORDER BY date ASC LIMIT 0,3";
$info1 = $connexion->query($sql1);
$resultat1 = $info1->fetchALL(PDO::FETCH_OBJ);

//Sélectionner les 3 concerts suivants
$sql2 = "SELECT C.idConcert as idConcert, G.nom as nom, G.genre as genre, G.imgCover as cover, C.ville as ville, C.date as date, C.heure as heure, C.prix as prix FROM concerts C JOIN groupes G ON G.idGroupe = C.idGroupe ORDER BY date ASC LIMIT 3,3";
$info2 = $connexion->query($sql2);
$resultat2 = $info2->fetchALL(PDO::FETCH_OBJ);

//Sélectionner tous les genres
$sql4 = "SELECT DISTINCT genre FROM groupes ORDER BY genre";
$info4 = $connexion->query($sql4);
$resultat4=$info4->fetchALL(PDO::FETCH_OBJ);

//Sélectionner toutes les villes
$sql5 = "SELECT DISTINCT ville FROM concerts ORDER BY ville";
$info5 = $connexion->query($sql5);
$resultat5 = $info5->fetchALL(PDO::FETCH_OBJ);

//Filtrer les concerts
if (isset($_POST['send'])) {
    $genre = htmlspecialchars($_POST['select-genre']);
    $avant_apres = htmlspecialchars($_POST['select-avant-apres']);
    $calendrier = htmlspecialchars($_POST['calendrier']);
    $ville = htmlspecialchars($_POST['select-ville']);
    $trie = htmlspecialchars($_POST['select-trie']);
    $ordre = htmlspecialchars($_POST['select-ordre']);


    if ($genre == "all" && $ville == "all") {
        $sql3 = "SELECT C.idConcert as idConcert, G.nom as nom, G.genre as genre, G.imgCover as cover, C.ville as ville, C.date as date, C.heure as heure, C.prix as prix FROM concerts C JOIN groupes G ON G.idGroupe = C.idGroupe WHERE";
    }
    elseif ($genre == "all" && $ville != "all") {
        $sql3 = "SELECT C.idConcert as idConcert, G.nom as nom, G.genre as genre, G.imgCover as cover, C.ville as ville, C.date as date, C.heure as heure, C.prix as prix FROM concerts C JOIN groupes G ON G.idGroupe = C.idGroupe WHERE ville = '$ville' AND";
    }


    elseif ($genre != "all" && $ville == "all") {
        $sql3 = "SELECT C.idConcert as idConcert, G.nom as nom, G.genre as genre, G.imgCover as cover, C.ville as ville, C.date as date, C.heure as heure, C.prix as prix FROM concerts C JOIN groupes G ON G.idGroupe = C.idGroupe WHERE G.genre = '$genre' AND";
    }
    else {
        $sql3 = "SELECT C.idConcert as idConcert, G.nom as nom, G.genre as genre, G.imgCover as cover, C.ville as ville, C.date as date, C.heure as heure, C.prix as prix FROM concerts C JOIN groupes G ON G.idGroupe = C.idGroupe WHERE G.genre = '$genre' AND ville = '$ville' AND";
    }

    if ($avant_apres == "avant") {
        $sql3 = $sql3." C.date <= '$calendrier' ORDER BY $trie $ordre";
    }
    else {
        $sql3 = $sql3." C.date >= '$calendrier' ORDER BY $trie $ordre";
    }

    $info3 = $connexion->query($sql3);
    $resultat3 = $info3->fetchALL(PDO::FETCH_OBJ);
}


?>



<!--------------------  H T M L  -------------------->

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Accueil | Top Concert</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link rel="shortcut icon" href="./img/favicon.png">
        <link rel="stylesheet" href="css/style-index.css">
    </head>
    <body>

        <header>
            <?php require('header.php') ?>
        </header>

        <!-- DERNIERS CONCERTS AJOUTÉS -->
        <section>
            <h2>C'est pour bientôt :</h2>

            <ul id="liste-concert" class="slideshow-container">

                <div class="mySlides fade">
                    <?php foreach ($resultat1 as $key => $value): ?>
                    <li>
                        <div class="filtre-bleu"></div>
                        <div class="cover-index" style="background-image: url('<?='img/groupes/'.$value->cover?>');"></div>
                        <h3 title="<?=$value->nom?>"><?=tronquer_texte($value->nom, 14)?></h3>
                        <p title="Genre : <?=$value->genre?>"><b>Genre :</b> <?=tronquer_texte($value->genre, 10)?></p>
                        <p title="Ville : <?=$value->ville?>"><b>Ville :</b> <?=tronquer_texte($value->ville, 11)?></p>
                        <p title="Date : <?=formater_date($value->date)?>"><b>Date :</b> <?=formater_date($value->date)?></p>
                        <p title="Prix : <?=$value->prix?>€"><b>Prix :</b> <?=$value->prix?>€</p>
                        <a href="index.php?idConcert=<?=$value->idConcert?>" title="Cliquez ici pour plus d'informations sur ce concert">EN SAVOIR PLUS</a>
                    </li>
                    <?php endforeach ?>
                </div>

                <div class="mySlides fade">
                    <?php foreach ($resultat2 as $key => $value): ?>
                    <li>
                        <div class="filtre-bleu"></div>
                        <div class="cover-index" style="background-image: url('<?='img/groupes/'.$value->cover?>');"></div>
                        <h3 title="<?=$value->nom?>"><?=tronquer_texte($value->nom, 14)?></h3>
                        <p title="Genre : <?=$value->genre?>"><b>Genre :</b> <?=tronquer_texte($value->genre, 10)?></p>
                        <p title="Ville : <?=$value->ville?>"><b>Ville :</b> <?=tronquer_texte($value->ville, 11)?></p>
                        <p title="Date : <?=formater_date($value->date)?>"><b>Date :</b> <?=formater_date($value->date)?></p>
                        <p title="Prix : <?=$value->prix?>€"><b>Prix :</b> <?=$value->prix?>€</p>
                        <a href="index.php?idConcert=<?=$value->idConcert?>" title="Cliquez ici pour plus d'informations sur ce concert">EN SAVOIR PLUS</a>
                    </li>
                    <?php endforeach ?>
                </div>

                <!-- Next and previous buttons -->
                <a class="prev" id="test" title="Précédent" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" title= "Suivant" onclick="plusSlides(1)">&#10095;</a>

            </ul>

        </section>


        <!-- VUE CONCERT -->
        <?php include("vue_concert.php"); ?>


        <!-- FILTRER LES CONCERTS -->
        <section>
            <h2>Trouvez votre concert :</h2>
            <form name="formulaire" method="post" action="index.php#liste-formulaire">
                <ul id="liste-formulaire">

                    <li id="liste-genre">
                        <label for="select-genre"><b>Sélectionnez le genre :</b></label>
                        <select name="select-genre" id="select-genre">
                            <option value="all" selected>Tous les genres</option>
                            <?php foreach ($resultat4 as $key => $value): ?>
                            <option value="<?=$value->genre?>"><?=$value->genre?></option>
                            <?php endforeach ?>
                        </select>
                    </li>

                    <li id="liste-ville">
                        <label for="select-ville"><b>Sélectionnez la ville :</b></label>
                        <select name="select-ville" id="select-ville">
                            <option value="all" selected>Toutes les villes</option>
                            <?php foreach ($resultat5 as $key => $value): ?>
                            <option value="<?=$value->ville?>"><?=$value->ville?></option>
                            <?php endforeach ?>
                        </select>
                    </li>

                    <li id="liste-date">
                        <label for="select-date"><b>Sélectionnez la date :</b></label>
                        <select name="select-avant-apres" id="select-date">
                            <option value="apres" selected>Après le</option>
                            <option value="avant">Avant le</option>
                        </select>
                        <input type="date" name="calendrier" value="<?=date('Y-m-d')?>" required>
                    </li>

                    <li id="liste-trie">
                        <label for="select-tire"><b>Trier par :</b></label>
                        <select name="select-trie" id="select-trie">
                            <option value="genre" selected>Genre</option>
                            <option value="ville">Ville</option>
                            <option value="date">Date</option>
                            <option value="prix">Prix</option>
                        </select>
                        <select name="select-ordre" id="select-ordre">
                            <option value="ASC" selected>(ordre croissant)</option>
                            <option value="DESC">(ordre décroissant)</option>
                        </select>
                    </li>

                    <li>
                        <input id="send" name="send" type="submit" value="VALIDER">
                    </li>

                </ul>			
            </form>
        </section>


        <!-- RÉSULTATS FILTRE -->

        <?php if (isset($_POST['send'])): ?>
        <?php if (empty($resultat3)): ?>
        <section>
            <h2>Aucun concerts trouvés...</h2>
            <p class="no-result">Essayez peut-être avec une autre date ?</p>
        </section>
        <?php else: ?>
        <section>
            <h2>Concerts trouvés :</h2>
            <ul id="liste-concert" class="trie">
                <div>
                    <?php foreach ($resultat3 as $key => $value): ?>
                    <li>
                        <div class="filtre-bleu"></div>
                        <div class="cover-index" style="background-image: url('<?='img/groupes/'.$value->cover?>');"></div>
                        <h3 title="<?=$value->nom?>"><?=tronquer_texte($value->nom, 14)?></h3>
                        <p title="Genre : <?=$value->genre?>"><b>Genre :</b> <?=tronquer_texte($value->genre, 10)?></p>
                        <p title="Ville : <?=$value->ville?>"><b>Ville :</b> <?=tronquer_texte($value->ville, 11)?></p>
                        <p title="Date : <?=formater_date($value->date)?>"><b>Date :</b> <?=formater_date($value->date)?></p>
                        <p title="Prix : <?=$value->prix?>€"><b>Prix :</b> <?=$value->prix?>€</p>
                        <a href="index.php?idConcert=<?=$value->idConcert?>" title="Cliquez ici pour plus d'informations sur ce concert">EN SAVOIR PLUS</a>
                    </li>
                    <?php endforeach ?>
                </div>
            </ul>
        </section>
        <?php endif ?>
        <?php endif ?>

        <footer>
            <?php require('footer.php') ?>
        </footer>




        <!-- SCRIPTS -->
        <script type="text/javascript" src="js/slide.js"></script>
        <script type="text/javascript" src="js/comp_index.js"></script>
        <script>console.log("eijdij");</script>

    </body>
</html>


<?php

    if(isset($_GET["idConcert"])) {

        //Selectionner informations concert

        echo "<script type=\"text/javascript\">";
        echo "document.getElementById(\"vue_concert\").style.display = \"inline-block\"; document.getElementById(\"filtre-noir\").style.display = \"block\";";
        echo "</script>";

    }

?>
