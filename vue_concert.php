<?php

if(isset($_GET["idConcert"])) {

    //Selectionner informations concert
    $idConcert = $_GET["idConcert"];
    
    $select_concert = "SELECT * FROM concerts C JOIN groupes G ON C.idGroupe = G.idGroupe WHERE idConcert = $idConcert";
    $select_concert = $connexion->query($select_concert);
    $select_concert = $select_concert->fetch(PDO::FETCH_OBJ);
    
    if(empty($select_concert))
        header("Location:index.php");

}

?>




<!DOCTYPE html>
<html lang="fr">

<head>
    <link rel="stylesheet" href="css/style-concert.css">
</head>

<body>

    <div id="vue_concert" class="container">

        <!-- Retour -->
        <header class="quitter" onclick="javascript:offAll()">
            <a title="Quitter" onclick="javascript:off('vue_concert')"><i class="fas fa-times"></i></a>
        </header>


        <!-- Carte -->
        <div class="card">

            <!-- Bandeau supérieur -->
            <div class="bandeau">

                <!-- Nom / Genre -->
                <h2><?=strtoupper($select_concert->nom)?></h2>
                <p><?=$select_concert->genre?></p>

                <!-- Profil -->
                <div class="profil" style="background-image: url('img/groupes/<?=$select_concert->imgAvatar?>');"></div>

                <!-- Cover -->
                <div class="cover" style="background-image: url('img/groupes/<?=$select_concert->imgCover?>');"></div>

            </div>

            <!-- Informations -->
            <div class="info">

                <table>
                    <tr>
                        <th><i class="fas fa-clock" title="Horaire"></i></th>
                        <th class="border"><i class="fas fa-map-marker-alt" title="Lieu"></i></th>
                        <th><i class="fas fa-money-bill" title="Prix"></i></th>
                    </tr>
                    <tr>
                        <td><?=formater_date($select_concert->date)?><br><?=formater_heure($select_concert->heure)?></td>
                        <td class="border"><?=$select_concert->lieu?>,<br><?=$select_concert->ville?></td>
                        <td><?=$select_concert->prix?>€<br>TTC</td>
                    </tr>
                    <tr>
                        <td colspan="3" class="bio"><?=$select_concert->bio?></td>
                    </tr>
                </table>

            </div>


            <!-- Bouton -->
            <div class="bouton">
                <a href="confirmation.php?idConcert=<?=$idConcert?>" title="Réserver pour participer au concert">RÉSERVER</a>
            </div>

        </div>

    </div>

    <div id="filtre-noir" onclick="javascript:offAll()"></div>

</body>

</html>
